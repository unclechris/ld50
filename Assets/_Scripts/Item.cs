using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewItem", menuName ="Item")]
public class Item : ScriptableObject
{
    public string itemName;
    public string SpawnLocation;
    public string ReturnLocation;
    public Sprite imageOfItem;
    public string SoundClipForitem;
    public GameObject buttonPrefab;
    public GameObject itemPrefab;
}
