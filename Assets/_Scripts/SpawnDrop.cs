using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDrop : MonoBehaviour
{
    public GameObject item;
    public Item myItem;
    private Transform player;
    public LayerMask itemLayer;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    public void SpawnDroppedItem()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(player.position, 1f, itemLayer);
        if (colliders.Length > 0)
        {
            bool nearLocation = false;
            int minDistColliderIndex = 0;
            float minDistCollider = float.MaxValue;
            //check if any are the drop off location
            for (int i = 0; i < colliders.Length; i++)
            {
                if(colliders[i].name == myItem.ReturnLocation)
                {
                    Debug.Log("YEAH!!");
                    nearLocation = true;
                    minDistColliderIndex = i;
                    minDistCollider = float.MinValue;
                }
                float distToCollider = Vector3.Distance(colliders[i].gameObject.transform.position, player.position);
                if (!nearLocation && distToCollider < minDistCollider)
                {
                    minDistColliderIndex = i;
                    minDistCollider = float.MinValue;
                }
            }
            Instantiate(item, colliders[minDistColliderIndex].transform.position, Quaternion.identity);
        }
        else
        {
            Vector2 playerPos = new Vector2(player.position.x - 1.5f, player.position.y);
            Instantiate(item, playerPos, Quaternion.identity);
        }
    }
}
