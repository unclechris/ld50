using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    private Inventory inventory;
    [SerializeField] private int i;
    // Start is called before the first frame update
    private void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }
    public void DropItem()
    {
        Debug.Log(transform.childCount);
        foreach (Transform child in transform)
        {
            child.GetComponent<SpawnDrop>().SpawnDroppedItem();
            GameObject.Destroy(child.gameObject);
        }
        inventory.isFull[0] = false;
        inventory.CleanUpSlots();
    }
}
