using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Pickup : MonoBehaviour
{
    private Inventory inventory;
    public GameObject itemButton;
    public bool pickUp = false;
    private void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        pickUp = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (pickUp)
        {
            PickupItem(collision);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (pickUp)
        {
            PickupItem(collision);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        pickUp = false;
    }

    public void OnInteraction(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            pickUp = true;
        }
    }
    private void PickupItem(Collider2D collision)
    {

        if (collision.CompareTag("Player"))
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (inventory.isFull[i] == false)
                {
                    //Add to Inventory
                    inventory.isFull[i] = true;
                    Instantiate(itemButton, inventory.slots[i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
        }

    }
}
