using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public Item[] objectsToSpawn;
    public Dictionary<string, Transform> spawnPoints = new Dictionary<string, Transform>();
    private void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform trans = transform.GetChild(i);
            spawnPoints.Add(trans.name, trans);
        }

    }
    private void Start()
    {
        for (int itemToSpawn = 0; itemToSpawn < objectsToSpawn.Length; itemToSpawn++)
        {
            Item curItem = objectsToSpawn[itemToSpawn];
            objectsToSpawn[itemToSpawn].itemPrefab.GetComponentInChildren<SpriteRenderer>().sprite = curItem.imageOfItem;
            objectsToSpawn[itemToSpawn].buttonPrefab.transform.GetChild(0).GetComponentInChildren<Image>().sprite = curItem.imageOfItem;
            objectsToSpawn[itemToSpawn].buttonPrefab.GetComponent<SpawnDrop>().myItem = curItem;
            Transform trans = null;
            if (spawnPoints.TryGetValue(objectsToSpawn[itemToSpawn].SpawnLocation,out trans))
            {                
                Instantiate(objectsToSpawn[itemToSpawn].itemPrefab, trans.position, trans.rotation);
            }
        }
           
    }
}
