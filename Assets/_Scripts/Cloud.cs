using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    private float CloudSpeed;
    private void Awake()
    {
        CloudSpeed = Random.Range(.2f, 3f);
    }
    void Update()
    {
        Vector2 curPos = transform.position;
        curPos.x += CloudSpeed * Time.deltaTime;
        if (curPos.x > 25)
        {
            curPos.x = -30f;
            curPos.y += Random.Range(-5.0f, 5.0f);
            curPos.y = Mathf.Clamp(curPos.y, -15f, 15f);
            CloudSpeed = Random.Range(.2f, 3f);
        }
        transform.position = curPos;
    }
}
